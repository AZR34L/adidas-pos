var WebSocketServer = require('ws').Server; 
var noble = require('noble');
var azure = require('azure');
var notificationHubService = azure.createNotificationHubService('samplehub','Endpoint=sb://samplehubnamespace.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=qwuenGBazY3WpJ6xp7H7bDpXmmQnp5eYsw8z0XJ17Tc=');
wss = new WebSocketServer({port: 8001});

var x = 0;
var y = 0;
var z = 0;
var id = null;

var shakeTime = 0;

var stickerObj;


console.log("chck");
noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning([], true);
    console.log("start");
  } else {
    noble.stopScanning();
        console.log("stop");
  }
});


function pushShake(){
    var tags = "BeaconNotify";
    var payload = {
        data: {
            message: 'Stop Shaking me!',
            orderid: "123"
        }
    };
    // PUSH
    notificationHubService.gcm.send(tags, payload, function(error, res){
    if(!error){
        console.log("Yay Message sent");
    }
    });
}

// "d0d3fa86ca7645ec9bd96af42426c561", "d0d3fa86ca7645ec9bd96af45b9a030c"
// 

noble.on('discover', function(peripheral) {
  if(peripheral.advertisement.manufacturerData){
    if(peripheral.advertisement.manufacturerData.slice(3, 11).toString('hex') == "2426c5618487b13a" || peripheral.advertisement.manufacturerData.slice(3, 11).toString('hex') == "5b9a030c9074887f"){

      id = peripheral.advertisement.manufacturerData.slice(3, 11).toString('hex');

  var moving = (peripheral.advertisement.manufacturerData.readUInt8(15) & 0x40) !== 0;
  var inX = peripheral.advertisement.manufacturerData.readInt8(16) * 15.625;
  var inY = peripheral.advertisement.manufacturerData.readInt8(17) * 15.625;
  var inZ = peripheral.advertisement.manufacturerData.readInt8(18) * 15.625;
  var shake;

if (Math.abs(inX) > Math.abs(inY)
 && Math.abs(inX) > Math.abs(inZ)){
   if(inX > 0){
      x = 1;
      z = 0;
      y = 0;
   }else if (inX == 0){
      x = 0;
      z = 0;
      y = 0;
   }else if (inX < 0){
      x = -1;
      z = 0;
      y = 0;
  }
 } else if (Math.abs(inY) > Math.abs(inX)
 && Math.abs(inY) > Math.abs(inZ)){
   if(inY > 0){
      x = 0;
      y = 1;
      z = 0;
   }else if (inY == 0){
      x = 0;
      y = 0;
      z = 0;
   }else if (inY < 0){
      x = 0;
      y = -1;
      z = 0;
  }
 }else if (Math.abs(inZ) > Math.abs(inY)
 && Math.abs(inZ) > Math.abs(inX)){
   if(inZ > 0){
      x = 0;
      y = 0;
      z = 1;
   }else if (inZ == 0){
      x = 0;
      y = 0;
      z = 0;
   }else if (inZ < 0){
      x = 0;
      y = 0;
      z = -1;
  }
 }


 if (Math.abs(inX) + Math.abs(inY) + Math.abs(inZ) > 2500){
   shake = true;
  }else {
    shake = false;
  }

    stickerObj = {"x":x, "y":y, "z":z, "id":id, "shake":shake};
    // if(moving){console.log(stickerObj.id);}
    // console.log(x + "\t\t" + y + "\t\t" + z);
    if (shake && (shakeTime + 1000 * 30 < (new Date()).getTime())){
      shakeTime = (new Date()).getTime();
      pushShake();
      console.log("shaking");
    }
    broadcast(JSON.stringify(stickerObj));
    }
  }
});

wss.on('connection', function(ws) {
    ws.on('message', function(message) {
        console.log('Msg received in server: %s ', message);
    });
}); 


function broadcast(stickerObj) {
    wss.clients.forEach(function each(client) {
        client.send(stickerObj);
    });
}