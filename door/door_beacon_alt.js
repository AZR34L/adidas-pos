var noble = require('noble');
var count = 0;
var prev = false;
var doorIsOpen = false;
var doorIsClosed = true;

console.log("chck");
noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning([], true);
    console.log("start");
  } else {
    noble.stopScanning();
        console.log("stop");
  }
});

function openDoor(){
    doorIsOpen = true;
    doorIsClosed = false;
}

function closeDoor(){
    doorIsOpen = false;
    doorIsClosed = true;
}

function increment(){
    count += 1;
}

noble.on('discover', function(peripheral) {
    if(peripheral.advertisement.manufacturerData){
      if(peripheral.advertisement.manufacturerData.slice(3, 11).toString('hex') == "41fc894f5eb23711"){

        var inZ = peripheral.advertisement.manufacturerData.readInt8(18);
        

        if (Math.abs(inZ) > 5 && doorIsClosed){
            openDoor();
            prev = true;
        }else{
            if (prev && doorIsClosed){
                increment();
                console.log(count);
                prev = false;
            }
            closeDoor();
        }

        


      }
    }
});


// chair: 5b9a030c9074887f shoe: 2426c5618487b13a door: 41fc894f5eb23711